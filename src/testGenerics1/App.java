package testGenerics1;

public class App {
    public static void main(String[] args) {
        MyTuple<String,Integer,Long> myTuple1 = new MyTuple<>("a", 1, 10L);
        MyTuple<Double,String,String> myTuple2 = new MyTuple<>(5.4, "a", "b");
    }
}
