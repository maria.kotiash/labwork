package labWork2_11.testexcp1;

import java.nio.file.AccessDeniedException;

public class MyTest {
    public void test() throws MyException {
        throw new MyException("Hello fron MyException");
    }

    public void testMyRT() {
        throw new MyRTException("Hello from MyRTException");

    }

    public void testDivisionZero(){
        throw new ArithmeticException(" / by 0");
    }

    public void testIOException() throws AccessDeniedException{
        throw new AccessDeniedException("Cannot");
    }
}
