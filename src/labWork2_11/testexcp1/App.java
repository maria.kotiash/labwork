package labWork2_11.testexcp1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {
        MyTest myTest = new MyTest();


        Path path = Paths.get("C:\\py", "file.txt");
        try {
            for(String string : Files.readAllLines(path)) {
                System.out.println(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            LOGGER.info("Bye");
        }


    }

}
