package labWork2_13.testEnum2;

public enum  Suit {
    SPADE("♤"),
    DIAMOND("♢"),
    CLUB("♧"),
    HEART("♥");

    private String symbol;
    private Suit(String symbol){
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }
}
