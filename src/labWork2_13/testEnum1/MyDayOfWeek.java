package labWork2_13.testEnum1;

public enum MyDayOfWeek {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY;

   public MyDayOfWeek nextName(){
        int nextNameId = this.ordinal() + 1;
        if( nextNameId == MyDayOfWeek.values().length){
            nextNameId = 0;
        }
        return MyDayOfWeek.values()[nextNameId];
   }
}
