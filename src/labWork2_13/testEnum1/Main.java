package labWork2_13.testEnum1;

import labWork2_13.testEnum2.Card;
import labWork2_13.testEnum2.Rank;
import labWork2_13.testEnum2.Suit;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(MyDayOfWeek.values()));

        for(MyDayOfWeek day : MyDayOfWeek.values()){
            switch (day){
                case MONDAY:
                case WEDNESDAY:
                case FRIDAY: {
                    System.out.printf("My Java day ", day, day.ordinal());
                    break;
                }
                default:
                    System.out.println("This is not Java day");
            }
        }

        MyDayOfWeek wednesday = MyDayOfWeek.WEDNESDAY;
        System.out.println(wednesday.nextName());

        Card[] cards = new Card[52];
        int idx = 0;
        for(Suit suit : Suit.values()){
            for(Rank rank : Rank.values() ){
                cards[idx++] = new Card(suit, rank);
            }
        }

        for(Card card : cards){
            System.out.println(card);
        }
    }
}
