package Lab1_2.work1;

import java.io.*;

public class SerializeDemo {
    public static final String EMPLOYEE_BIN = "employee.bin";
    public static void main(String[] args) {
        Employee employee = new Employee("Maria", "Ternopil", 2000, 5000);
        try(ObjectOutputStream outputStream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(EMPLOYEE_BIN)))){
            outputStream.writeObject(employee);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
