package labWork2_15;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TestCollection2 {
    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        for(int i = 0; i < 10; i++) {
           list.add("number_" + i);
        }
        Collections.shuffle(list);

        list.forEach(System.out::println);
    }
}
