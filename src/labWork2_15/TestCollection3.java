package labWork2_15;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TestCollection3 {
    public static void main(String[] args) {
        List<String> strings =  new ArrayList<>();
        List<String> strings2 = new LinkedList<>();

        for(int i = 0; i < 10; i++) {
            strings.add("number_" + i);
            strings2.add("number_" + i);
        }

        ThreadLocalRandom random = ThreadLocalRandom.current();
        for(int i = 0; i < strings.size(); i++){
            strings2.add(random.nextInt(strings2.size()), strings.get(i));
        }

        for(Iterator<String> iterator1 = strings2.iterator(); iterator1.hasNext();){
            String str = iterator1.next();
            System.out.println(str);
        }

    }
}
