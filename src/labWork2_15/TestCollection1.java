package labWork2_15;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TestCollection1 {
    public static void main(String[] args) {
        List<String> strings =  new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            strings.add("number" + i++);
        }

        strings.add("aaaaaa");
        strings.add("bbbbbb");
        strings.add("cccccc");
        strings.add("dddddd");
        Iterator<String> iterator = strings.iterator();
        ListIterator<String> iterator2 = strings.listIterator(strings.size());

        while(iterator2.hasNext()) {
            System.out.println(iterator2.next());
        }

        for(Iterator<String> iterator1 = strings.iterator(); iterator1.hasNext();){
            String str = iterator1.next();
            System.out.println(str);
        }

        for(String s : strings){
            System.out.println(s);
        }
    }
}
