package labWork2_15;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTranslator {
    Map<String, String> dictionary = new HashMap<>();

    public void addNewWord(String en, String ua){
        dictionary.put(en, ua);

    }
    public String translate(String en){
        if(en.split(" ").length == 1){
            return dictionary.get(en);
        } else{
            String result = "";
            List<String> word = Arrays.asList(en.split(" "));
            for(String all : word){
                result += dictionary.get(all) + " ";
            }
            return result.trim();
        }

    }

    public static void main(String[] args) {
        MyTranslator myTranslator = new MyTranslator();
        myTranslator.addNewWord("cat", "кіт");
        myTranslator.addNewWord("fox", "лис");
        myTranslator.addNewWord("dog", "пес");
        myTranslator.addNewWord("bird", "птах");
        myTranslator.addNewWord("flying", "літає");
        myTranslator.addNewWord("barking", "гавкає");
        System.out.println(myTranslator.dictionary);

        System.out.println(myTranslator.translate("bird"));
        System.out.println(myTranslator.translate("dog"));
        System.out.println(myTranslator.translate("dog barking"));
        System.out.println(myTranslator.translate("bird flying"));
    }
}
