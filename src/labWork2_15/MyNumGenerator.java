package labWork2_15;

import java.util.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class MyNumGenerator {
    int numOfElm;
    int maxNumb;

    public MyNumGenerator(int numOfElm, int maxNumb) {
        this.numOfElm = numOfElm;
        this.maxNumb = maxNumb;
    }

    public Set<Integer> generateDistinct(){
        Set<Integer> result = new HashSet<>();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        while(result.size() <= numOfElm)
        {
            result.add(random.nextInt(maxNumb));
        }
        return result;
    }

   public List<Integer> generate(){
        List<Integer> result = new ArrayList<>();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for(int i =0; i < numOfElm; i++){
            result.add(random.nextInt(maxNumb));
        }
        return result;
    }

    public static void main(String[] args) {
        MyNumGenerator myNumGenerator = new MyNumGenerator(5, 64);
        List<Integer> values = myNumGenerator.generate();
        System.out.println(values);
    }
}
