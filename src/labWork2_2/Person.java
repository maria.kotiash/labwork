package labWork2_2;

public class Person {
    public String firstName;
    public String lastName;
    public int age;
    public String gender;
    public int phoneNumber;

    public void setPerson(String firstName){
        this.firstName = firstName;
    }

    public void setPerson(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void setPerson(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public void setPerson(String firstName, String lastName, int age, String gender){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
    }
    public void setPerson(String firstName, String lastName, int age, String gender, int phoneNumber){
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
    }
}
