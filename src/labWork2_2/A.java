package labWork2_2;

public class A {
    void calcSquare(final int side) {
    System.out.println("Square of rectangle " + side * side);
}

void calcSquare(final int side, final int side2){
    System.out.println("Square of foursquare " + side * side2);
}

void calcSquare(final double radius){
        double result = Math.PI * radius * radius;
    System.out.println("Square of circle " + result);
}
    public static void main(String[] args) {
     A a = new A();
     a.calcSquare(4);
     a.calcSquare(8, 14);
     new A().calcSquare(6.6);
    }
}
