package labWork2_2;

public class Employee {
    public double calcSalary(String name, double...salaries) {
        double totalSalary = 0;
        for(double salary : salaries) {
            totalSalary += salary;
        }
        System.out.println("Employee: " + name + " has salsry: " + totalSalary);
        return totalSalary;
    }
}
