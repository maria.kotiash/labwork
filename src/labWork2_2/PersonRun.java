package labWork2_2;

public class PersonRun {
    public static void main(String[] args) {
        Person person = new Person();
        person.setPerson("Maria");
        person.setPerson("Maria", "Kotiash");
        person.setPerson("Maria", "Kotiash", 22);
        person.setPerson("Maria","Kotiash",22, "Female");
        person.setPerson("Maria","Kotiash",22, "Female", 8774798);
    }
}
