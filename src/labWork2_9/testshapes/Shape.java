package labWork2_9.testshapes;


public class Shape {
    private String shapeColor;

    public Shape () {

    }

    public String getShapeColor() {
        return shapeColor;
    }

    public Shape(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    public double calcArea() {
        return 1;
    }

    @Override
    public String toString() {
        return "Shape, color is: " + shapeColor;
    }



}
