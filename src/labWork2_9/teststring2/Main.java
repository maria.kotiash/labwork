package labWork2_9.teststring2;

public class Main {
    public static void main(String[] args) {
        String myStr1 = "Cartoon";
        String myStr2 = "Tomcat";
        char[] mS1 = myStr1.toCharArray();
        char[] mS2 = myStr2.toCharArray();

        for(int i = 0; i < mS1.length; i++){
            int count = 0;
            for(int j = 0; j < mS2.length; j++){
                if(mS1[i] == mS2[j]){
                    count++;
                }
            }
            if(count == 0){
                System.out.print(mS1[i] + " ");
            }
        }
    }
}
