package labWork2_9.testTokennizer;

import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        String myStr = "This is String, split by StringTokenizer. Created by Student:Name of Student";
        String delimiters = "., ";
        StringTokenizer tokenizer = new StringTokenizer(myStr, delimiters);
        while(tokenizer.hasMoreElements()){
            System.out.println(tokenizer.nextToken());
        }
    }
}
