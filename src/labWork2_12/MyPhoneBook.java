package labWork2_12;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhoneBook {
    public PhoneNumber[] PhoneNumbers() {
        return new PhoneNumber[0];
    }

    static class NamesComparator implements Comparator {
        @Override
        public int compare(Object o1, Object o2) {
            String name1 = (String)o1;
            String name2 = (String)o2;
            return name1.compareTo(name2);
        }
    }

    private class PhoneComparator implements Comparator <String> {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    }

static class  PhoneNumbersComparator implements Comparator<PhoneNumber>{

    @Override
    public int compare(PhoneNumber o1, PhoneNumber o2) {
        return o1.name.compareTo(o2.name);
    }
}

    static class PhoneBookOverflowException extends RuntimeException{
    public PhoneBookOverflowException(String message){
        super (message);
    }
    }

    public void sortByName(){
        Arrays.sort(phoneNumbers, new NamesComparator());
    }

    public void sortByPhoneNumbers(){
        Arrays.sort(phoneNumbers, new PhoneNumbersComparator());
    }


    private int phoneNumberIndex = 0;
    private PhoneNumber[] phoneNumbers = new PhoneNumber[10];

    public void addPhoneNumber(String name, String phone){
        if(phoneNumberIndex > phoneNumbers.length - 1){
            System.err.println("Your phone book is overflowed");
throw new PhoneBookOverflowException("numbers: " + phoneNumberIndex);
        }
        phoneNumbers[phoneNumberIndex++] = new PhoneNumber(name, phone);

    }

    public void printPhoneBook(){
        for(PhoneNumber number : phoneNumbers){
            if(number != null){
                System.out.println(number);
            }
        }
    }


    static class PhoneNumber{
String name;
String phone;

        public PhoneNumber(String name, String phone) {
            this.name = name;
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public String getPhone() {
            return phone;
        }

        @Override
        public String toString() {
            return "PhoneNumber{" +
                    "name='" + name + '\'' +
                    ", phone='" + phone + '\'' +
                    '}';
        }
    }
}
