package labWork2_12;

import java.util.Arrays;
import java.util.Comparator;

public class MyPhone {

    class Camera{}
    class Bluetooth{}
    static class MemoryCard{}
    static class SimCard{}
    class PowerOnButton{}
    static class HeadPhones{}
    class PhoneBattery{}
    static class PhoneCharger{}
    class PhoneDisplay{}
    class PhoneSpeaker{}


    private MyPhoneBook phoneBook;
    private  boolean switchOn;

    public MyPhone() {
        this.phoneBook = phoneBook;
    }

    public void switchOn(){
        System.out.println("Loading...");
        phoneBook.addPhoneNumber("Andriy", "38095656475");
        phoneBook.addPhoneNumber("Anna", "38095656868");
        System.out.println("OK");
        switchOn = true;
    }

    public void call(int indexInPhoneBook){
        if(switchOn == false){
            switchOn();
        }
        MyPhoneBook.PhoneNumber[] phoneNumbers = phoneBook.PhoneNumbers();
        MyPhoneBook.PhoneNumber phoneNumber = phoneNumbers[indexInPhoneBook];
        System.out.println("calling: " + phoneNumber);

        System.out.println("calling: " + phoneBook.PhoneNumbers()[indexInPhoneBook]);
    }



        private class NamesComparator implements Comparator {
            @Override
            public int compare(Object o1, Object o2) {
                String name1 = (String) o1;
                String name2 = (String) o2;
                return name1.compareTo(name2);
            }
        }

        private class PhoneComparator implements Comparator<String> {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        }

        private class PhoneNumbersComparator implements Comparator<labWork2_12.MyPhoneBook.PhoneNumber> {

            @Override
            public int compare(labWork2_12.MyPhoneBook.PhoneNumber o1, labWork2_12.MyPhoneBook.PhoneNumber o2) {
                return o1.name.compareTo(o2.name);
            }
        }

        private class PhoneBookOverflowException extends RuntimeException {
            public PhoneBookOverflowException(String message) {
                super(message);
            }
        }

        public void sortByName() {
            Arrays.sort(phoneNumbers, new labWork2_12.MyPhoneBook.NamesComparator());
        }

        public void sortByPhoneNumbers() {
            Arrays.sort(phoneNumbers, new labWork2_12.MyPhoneBook.PhoneNumbersComparator());
        }


        private int phoneNumberIndex = 0;
        private labWork2_12.MyPhoneBook.PhoneNumber[] phoneNumbers = new labWork2_12.MyPhoneBook.PhoneNumber[10];

        public void addPhoneNumber(String name, String phone) {
            if (phoneNumberIndex > phoneNumbers.length - 1) {
                System.err.println("Your phone book is overflowed");
                throw new labWork2_12.MyPhoneBook.PhoneBookOverflowException("numbers: " + phoneNumberIndex);
            }
            phoneNumbers[phoneNumberIndex++] = new labWork2_12.MyPhoneBook.PhoneNumber(name, phone);

        }

        public void printPhoneBook() {
            for (labWork2_12.MyPhoneBook.PhoneNumber number : phoneNumbers) {
                if (number != null) {
                    System.out.println(number);
                }
            }
        }


         class PhoneNumber {
            String name;
            String phone;

            public PhoneNumber(String name, String phone) {
                this.name = name;
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public String getPhone() {
                return phone;
            }

            @Override
            public String toString() {
                return "PhoneNumber{" +
                        "name='" + name + '\'' +
                        ", phone='" + phone + '\'' +
                        '}';
            }
        }

    }

