package labWork2_12;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

public class MyTestClass {
    public MyTestClass() {
        System.out.println("MyTestClass");
    }

    public void test(){
        System.out.println("test");
    }

    public static class V{
        public V() {
            System.out.println("V");
        }
    }

    class MyInner{

        public MyInner() {
            System.out.println("MyInner");
        }
    }

    public void local() {
        System.out.println("local");

        class MyLocal {
            public MyLocal() {
                System.out.println("MyLocal");
            }
        }
    }

    public static void main(String[] args) {
        MyTestClass c1 = new MyTestClass();
        c1.test();
        MyTestClass.MyInner inner = new MyTestClass().new MyInner();
        c1.local();
        MyTestClass.V v = new MyTestClass.V();



    }
}
