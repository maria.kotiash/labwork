package labWork2_4;

public class Employee {
    private String firstName;
    private String lastName;
    private String occupation;
    private String telephone;
    public static long numberOfEmployee;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public static long getNumberOfEmployee() {
        return numberOfEmployee;
    }

    public static void setNumberOfEmployee(long numberOfEmployee) {
        Employee.numberOfEmployee = numberOfEmployee;
    }

    public Employee(String firstName, String lastName, String occupation, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.occupation = occupation;
        this.telephone = telephone;
        numberOfEmployee++;
    }
}
