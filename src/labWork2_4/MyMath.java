package labWork2_4;

public class MyMath {
    final static double PI = 3.14;

    public static double areaOfCircle(int radius){
       return MyMath.PI * radius * radius;
    }

    public static double findMin(double[] values){
        double min = values[0];
        for(int i = 1; i < values.length; i++) {
            min = values[i] < min ? values[i] : min;
        }
        return min;
    }

    public static double findMin(int[] values){
        double min = values[0];
        for(int i = 1; i < values.length; i++) {
            min = values[i] < min ? values[i] : min;
        }
        return min;
    }

    public static double findMax(double[] values){
        double max = values[0];
        for(int i = 1; i < values.length; i++) {
            max = values[i] > max ? values[i] : max;
        }
        return max;
    }

    public static double findMax(int[] values){
        double max = values[0];
        for(int i = 1; i < values.length; i++) {
            max = values[i] > max ? values[i] : max;
        }
        return max;
    }

}
