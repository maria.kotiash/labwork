package labWork2_4;

public class MyPyramid {
    public static void printPyramid(int h) {
        String s = "";
        for(int i = 1; i <= h; i++) {
            for(int j = 1; j <= h - i; j++){
                System.out.print(" ");
            }
            for(int k = 1; k <= i - 1; k++) {
                System.out.print(k);
            }
            s = i + s;
            System.out.println(s);
        }
    }
    public static void main(String[] args) {
        printPyramid(8);
    }
}

