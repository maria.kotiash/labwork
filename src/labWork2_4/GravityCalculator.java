package labWork2_4;

public class GravityCalculator {
    static final double a = -9.81;
    static double v = 0;
    static double xi = 0;
    public static double calcDist(double t) {
        double x;
        return x = (0.5 * a * (t * t)) + (v * t) + xi;
    }

    public static void main(String[] args) {
        System.out.println(calcDist(15));
    }
}
