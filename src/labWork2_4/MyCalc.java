package labWork2_4;

public class MyCalc {
    public static double calcPi(int series){
        double pi = 0;
        for (int j = 1, i = 1; i < series ; j++, i+=2) {
            pi += (j % 2 != 0) ? 4.0 / i : 4.0 / i * (-1);
            System.out.printf("j=%d i=%d pi=%f sign=%d \n", j, i, pi, (j % 2 !=0) ? 1 : -1);
        }
        return  pi;
    }

    public static void main(String[] args) {
        double pi = calcPi(250);
        System.out.println(pi);
    }
}
