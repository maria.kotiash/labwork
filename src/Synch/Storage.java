package Synch;

public class Storage {
    private int value = 0;

    public void setValue(int newValue){
        value = newValue;
        try {
            System.out.println("Adding...");
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void minusValue(int val){
        value = value - val;
        try {
            System.out.println("Subtracting...");
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        Storage storage = new Storage();
    }


}
