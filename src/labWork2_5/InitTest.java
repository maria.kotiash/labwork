package labWork2_5;

import java.util.Arrays;

public class InitTest {
    private int id;
    private static int nextId = (int) (1000 * Math.random()) ;

    {
        id += nextId;
        nextId++;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "InitTest{" +
                "id=" + id +
                '}';
    }

    public static void main(String[] args) {
        InitTest[] init = new InitTest[10];

        for(int i = 0; i < init.length; i++){
            init[i] = new InitTest();
        }

        System.out.println(Arrays.toString(init));

    }
}
