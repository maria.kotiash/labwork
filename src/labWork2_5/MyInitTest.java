package labWork2_5;

public class MyInitTest {
    public int constructor;

    static{
        System.out.println("Static initialization block 1");
    }

    static{
        System.out.println("Static initialization block 2");
    }

    {
        System.out.println("Non-static initialization block 1");
    }

    {
        System.out.println("Non-static initialization block 2");
    }

    MyInitTest(){
        this(5);
        System.out.println("Constructor 2");
    }
    MyInitTest(int constructor){
        System.out.println("Constructor 1");
    }
}
