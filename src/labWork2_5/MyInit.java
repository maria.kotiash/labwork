package labWork2_5;

import java.util.Arrays;

public class MyInit {
    public static int[] arr = new int[10];

    static {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (100 * Math.random());
        }
    }
    void printArray(){
        System.out.println(Arrays.toString(arr));
    }

    public static void main(String[] args) {
         MyInit my = new MyInit();
         my.printArray();
    }
}
