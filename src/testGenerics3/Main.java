package testGenerics3;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
Integer[] integers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
String[] strings = {"one", "two", "three","four", "five", "six", "seven", "eight", "nine"};

MyMixer<Integer> integer = new MyMixer<>(integers);
MyMixer<String> string = new MyMixer<>(strings);

Integer[] mixInteger = integer.shuffle();
String[] mixString = string.shuffle();
        System.out.println(Arrays.toString(mixInteger));
        System.out.println(Arrays.toString(mixString));
    }
}
