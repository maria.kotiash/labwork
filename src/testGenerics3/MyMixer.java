package testGenerics3;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class MyMixer<T> {
    private T[] array;

    Comparator<T> myComparator = (t1, t2) -> t1.hashCode() - t2.hashCode();
    Comparator<T> myComparator2 = Comparator.comparingInt(Objects::hashCode);

    public MyMixer(T[] array){
        this.array = array;
    }

    public T[] shuffle(){
        T[] resultArray = Arrays.copyOf(array, array.length);
Arrays.sort(resultArray, myComparator);
        return  resultArray;
        }
    }

