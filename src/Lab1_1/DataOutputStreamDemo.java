package Lab1_1;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.ThreadLocalRandom;

public class DataOutputStreamDemo {
    public static void main(String[] args) {
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
       // String[] names = {"Maria", "Roman", "Yurii"};
        try (PrintWriter fw = new PrintWriter(new File("C:\\Users\\1\\file2.txt"));
             DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("C:\\Users\\1\\file2.bin")))) {
            for (int i = 0; i < 1_000_000; i++) {
                //fw.write(threadLocalRandom.nextFloat() + "\n");
                float val = (float) threadLocalRandom.nextGaussian();
                fw.write(val + "\n");
                dos.writeFloat(val);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        Path p = Paths.get("C:\\Users\\1\\file2.txt");
        try {
            System.out.println(Files.size(p) / 1024 / 1024 + "MBytes");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
