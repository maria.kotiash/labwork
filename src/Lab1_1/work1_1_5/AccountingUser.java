package Lab1_1.work1_1_5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class AccountingUser {
    private RandomAccessFile file;

    public AccountingUser() {
        File files = new File("C:\\Users\\1\\file.txt");
        try {
            file = new RandomAccessFile("C:\\Users\\1\\file.txt", "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    void testUsers(String nameUser) throws IOException {
        if (file.length() == 0) {
            file.writeBytes(nameUser + ":" + 1 + "\n");
            file.seek(0);
        } else if (file.length() > 0) {
            String line = null;
            long cursor = file.getFilePointer();
            while ((line = file.readLine()) != null) {
                cursor = file.getFilePointer();
                String userName = line.split(":")[0];
                int hits = Integer.valueOf(line.split(":")[1]);
                if (userName.equals(nameUser)) {
                    hits++;
                    file.seek(cursor);
                    file.writeBytes(userName + ":" + hits);
                    break;
                }
            }
            file.writeBytes(nameUser + ":" + 1 + "\n");
            file.seek(0);
        }
    }

    void printFile() throws IOException {
        file.seek(0);
        String line = null;
        while ((line = file.readLine()) != null) {
            System.out.println(line);
        }
    }
}

