package Lab1_1;
import java.io.File;

public class MyFileList {
    public static void main(String[] args) {

        File file = new File("C:\\Users\\1\\IdeaProjects\\Game");
        if(file.exists() && file.isDirectory()){
            for(String fileName : file.list()){
                System.out.println(fileName);
            }
        }
        System.out.println(File.pathSeparator);
        System.out.println(File.separator);
        File[] roots = File.listRoots();
        for(File f : roots){
            System.out.println(f.getAbsolutePath());
        }
    }
}
