package Lab1_1;

import java.io.*;

public class MyFileCopy {
    private static final String SRC_FILE = "C:\\Users\\1\\Pictures\\Saved Pictures\\kor.jpg";
    private static final String DST_FILE = "C:\\Users\\1\\Pictures\\kor.png";

    public static void main(String[] args) {
        File srcFile = new File(SRC_FILE);
        File dstFile = new File(DST_FILE);
        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(srcFile));
             OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(dstFile))) {
            int c;
            while (((c = inputStream.read()) != -1)) {
                outputStream.write(c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
