package Lab1_1;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class PrintFile {
    private static final Path path = Paths.get("C:\\Users\\1\\file2.txt");
    private static final File file = path.toFile();

    public static void main(String[] args) {
//        Scanner scanner = null;
//        try {
//            scanner = new Scanner(file);
//            while (scanner.hasNextLine()){
//                System.out.println(scanner.nextLine());
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }finally {
//            if(scanner != null) scanner.close();
//        }

        try(BufferedReader br = new BufferedReader(new FileReader(file))){
            String s;
            while ((s = br.readLine()) != null){
                System.out.println(s);
            }
        } catch (IOException e){
            e.printStackTrace();
        }



//        List<String> strings = null;
//        try {
//            strings = Files.readAllLines(path);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        strings.forEach(System.out::println);

    }
}
