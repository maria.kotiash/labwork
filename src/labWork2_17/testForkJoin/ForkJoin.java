package labWork2_17.testForkJoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ThreadLocalRandom;

public class ForkJoin {
    static private int[] array = ThreadLocalRandom.current()
            .ints(0, 100)
            .limit(1_000_000)
            .toArray();
static class SumOfSubArray extends RecursiveTask<Long>{
    int from;
    int to;
    int threshHold = 20;

    public SumOfSubArray(int from, int to) {
        super();
        this.from = from;
        this.to = to;
    }

    @Override
    protected Long compute() {
        long computedSum = 0;
        if(to - from <= 20){
            for (int i = from; i <= to; i++) {
                computedSum += array[i];
            }
            return computedSum;
        } else{
            int mid = (from + to) / 2;
            System.out.printf("[%d to %d] and [%d to %d]\n", from, mid, mid, to);
            SumOfSubArray left = new SumOfSubArray(from, mid);
            SumOfSubArray right = new SumOfSubArray(mid + 1, to);
            invokeAll(left, right);

            return left.join() + right.join();
        }



    }
}


    public static void main(String[] args) {
        SumOfSubArray task = new SumOfSubArray(0, array.length - 1);
        ForkJoinPool fj = new ForkJoinPool(Runtime.getRuntime().availableProcessors() * 2);
        fj.invoke(task);
        System.out.println("Sum: " + task.join());

    }
}
