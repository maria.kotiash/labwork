package labWork2_17.testConcurrentCollection;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) {
       // Map<UUID, String> map = new Hashtable<>(); //ConcurrentHashMap<>();   //HashMap<>();
        List<UUID> map = new CopyOnWriteArrayList<>();
        long start = System.currentTimeMillis();
        ExecutorService executorService = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < 100 ; i++) {
            executorService.submit(new CollectionReader(map));
            executorService.submit(new CollectionWriter(map));
        }
            long end = System.currentTimeMillis();
        executorService.shutdown();

        System.out.println("Time " + (end - start));
    }
}
