package labWork2_17.testConcurrentCollection;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CollectionWriter implements Runnable{
    private List<UUID> collection;

    public CollectionWriter(List<UUID> collection) {
        this.collection = collection;
    }

    @Override
    public void run() {
       // synchronized (collection)
            UUID uuid = UUID.randomUUID();
            collection.add(uuid);

    }
}
