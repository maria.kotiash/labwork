package labWork2_17.testConcurrentCollection;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CollectionReader implements Runnable{
    private List<UUID> collection;

    public CollectionReader(List<UUID> collection) {
        this.collection = collection;
    }

    public void setCollection(List<UUID> collection) {
        this.collection = collection;
    }

    @Override
    public void run() {
       // synchronized (collection)
            collection.get(0);

    }
}
