package labWork2_17;

public class MySumRun {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        MySumCount t1 = new MySumCount();
        MySumCount t2 = new MySumCount();
        t1.setArray(array);
        t2.setArray(array);
        t1.setStartIndex(0);
        t2.setStartIndex(array.length / 2);
        t1.setStopIndex(array.length / 2);
        t2.setStopIndex(array.length);

        t1.start();
        t2.start();
        try {
            t1.join();
            t1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Sum of elements " + (t1.getResultSum() + t2.getResultSum()));
    }
}
