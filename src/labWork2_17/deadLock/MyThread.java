package labWork2_17.deadLock;

import static labWork2_17.deadLock.MyObject.*;

public class MyThread extends Thread {


    MyThread t1 = new MyThread() {
            @Override
            public void run() {
                synchronized (resource1) {
                    System.out.println("Thread 1: Holding lock 1...");
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                    }
                    System.out.println("Waiting for lock 2...");
                    synchronized (resource2) {
                        System.out.println("Thread 1: locked resource 2");
                    }
                }
            }
        };



    MyThread t2 = new MyThread() {
        public void run() {
            synchronized(resource2) {
                System.out.println("Thread 2: Holding lock 2...");
                try { Thread.sleep(50); } catch (InterruptedException e) {}
                System.out.println("Waiting for lock 1...");
                synchronized(resource1) {
                    System.out.println("Thread 2: locked resource 1");
                }
            }
        }
    };

    MyThread t3 = new MyThread() {
        public void run() {
            synchronized (resource3) {
                System.out.println("Thread 3: Holding lock 3...");
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) { }
                System.out.println("Waiting for lock 2...");
                synchronized (resource2) {
                    System.out.println("Thread 3: locked resource 2");
                }
            }
        }
    };

}

