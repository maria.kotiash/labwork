package labWork2_17;

public class MySumCountRunnable implements Runnable {
    private int startIndex = 0;
    private int stopIndex = 0;
    private int[] array;
    private long resultSum = 0;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getStopIndex() {
        return stopIndex;
    }

    public void setStopIndex(int stopIndex) {
        this.stopIndex = stopIndex;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public long getResultSum() {
        return resultSum;
    }

    @Override
    public void run() {
        for(int i = startIndex; i < stopIndex; i++){
            resultSum += array[i];
        }
    }


    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        MySumCountRunnable t1 = new MySumCountRunnable();
        MySumCountRunnable t2 = new MySumCountRunnable();
        t1.setArray(array);
        t2.setArray(array);
        t1.setStartIndex(0);
        t2.setStartIndex(array.length / 2);
        t1.setStopIndex(array.length / 2);
        t2.setStopIndex(array.length);


Thread sum = new Thread(t1);
Thread sum2 = new Thread(t2);
        sum.start();
        sum2.start();
        try {
            sum.join();
            sum2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Sum of elements " + (t1.getResultSum() + t2.getResultSum()));
    }
}
