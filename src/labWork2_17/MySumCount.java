package labWork2_17;

public class MySumCount extends Thread {
    private int startIndex = 0;
    private int stopIndex = 0;
    private int[] array;
    private long resultSum = 0;

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getStopIndex() {
        return stopIndex;
    }

    public void setStopIndex(int stopIndex) {
        this.stopIndex = stopIndex;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public long getResultSum() {
        return resultSum;
    }

    @Override
    public void run() {
        for(int i = startIndex; i < stopIndex; i++){
            resultSum += array[i];
        }
    }
}
