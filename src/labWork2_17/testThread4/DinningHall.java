package labWork2_17.testThread4;

public class DinningHall {
    static int pizzaNum;
    static int studentID = 1;

    public synchronized void makePizza(){
        pizzaNum++;
        this.notifyAll();
        System.out.println("Pizza cooked");
    }

    public synchronized void servePizza(){
        if(pizzaNum < studentID){}
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String result;
        if(pizzaNum > 0){
            result = "Served";
            pizzaNum--;
        }
        else {
            result = "Starved";
        }
        System.out.println(result + studentID);
        studentID++;
    }

    public static void main(String[] args) {
        DinningHall hall = new DinningHall();
        for (int i = 0; i < 10; i++) {
          hall.makePizza();
        }
        for (int j = 1; j <= 20; j++) {
            hall.servePizza();
        }
    }
}
