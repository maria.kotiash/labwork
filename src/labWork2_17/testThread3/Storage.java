package labWork2_17.testThread3;

public class Storage {
    private int value;
    private boolean updated;

    public int getValue() {
        this.updated = false;
        return value;

    }

    public void setValue(int value) {
        this.value = value;
        this.updated = true;
    }

    public boolean isUpdated(){
        return false;
    }


}
