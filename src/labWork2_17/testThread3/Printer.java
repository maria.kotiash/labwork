package labWork2_17.testThread3;

public class Printer implements Runnable{
    private Storage storage;

    public Printer(Storage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        synchronized (storage){
            int value = storage.getValue();
            while (!storage.isUpdated()){
                try {
                    storage.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Printer..." + value);
            }
        }

    }
}
