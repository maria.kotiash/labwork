package labWork2_17.testThread3;

public class Counter implements Runnable {
    private Storage storage;

    public Counter(Storage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        for (int j = 0; j < 1000 ; j++) {
            System.out.println("Counter..." + j);
            synchronized (storage){
                storage.setValue(j);
                storage.notifyAll();
            }

        }

    }
}
