package labWork2_17.testThread3;

public class App {
    public static void main(String[] args) {
        Storage storage = new Storage();
        Thread counter = new Thread(new Counter(storage));
        Thread printer = new Thread(new Counter(storage));

        counter.start();
        printer.start();
    }
}
