package labWork2_1;

public class ArrComputer {
    public static void main(String[] args) {
        Computer dell = new Computer("dell", 1234, 500F, 45, 24);
        Computer hp = new Computer("hp", 312, 100f, 12, 13);
        Computer apple = new Computer("Apple", 234, 1000, 23, 15);
        Computer asus = new Computer("Asus", 214, 800, 57, 46);
        Computer sony = new Computer("Sony", 236, 700, 45, 66);

        Computer[] arr = new Computer[5];
        arr[0] = dell;
        arr[1] = hp;
        arr[2] = apple;
        arr[3] = asus;
        arr[4] = sony;


        for(Computer all : arr ) {
            all.view();
        }
    }

}