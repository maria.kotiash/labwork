package labWork2_16;

public class LabWork3 {
    static void foo(int a, int b){
        System.out.println("int and int");
    }

    static void foo(int... a){
        System.out.println("int...");
    }

    public static void main(String[] args) {
        int a = 6;
        foo(a, a);
        foo(a, a, a);
    }
}
