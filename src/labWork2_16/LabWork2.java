package labWork2_16;

public class LabWork2 {
    static void foo(int a){
        System.out.println("int");
    }

    static void foo(Byte a){
        System.out.println("Byte");
    }

    public static void main(String[] args) {
        byte b = 5;
        foo(b);
    }
}
