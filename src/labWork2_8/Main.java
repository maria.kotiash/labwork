package labWork2_8;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
   /**  Shape shape = new Shape("Red") {
        };
        System.out.println(shape);
        System.out.println(shape.calcArea());
        System.out.println();

        Shape circle = new Circle("Green", 22);
        System.out.println(circle);
        System.out.println(circle.calcArea());
        System.out.println();*/

        Shape rectangle = new Rectangle("Blue", 11, 22);
        System.out.println(rectangle);
        System.out.println(rectangle.calcArea());
        System.out.println();

        Shape triangle = new Triangle("Orange", 5, 5,5);
        System.out.println(triangle);
        System.out.println(triangle.calcArea());
        System.out.println();

        Shape[] shapes = {new Rectangle("Blue", 11, 22), new Rectangle("Green", 16, 14),
                new Rectangle("Red", 10, 23),
                new Rectangle("Black", 17, 20), new Circle("Red", 20),
                new Circle("Yellow", 18), new Triangle("Red", 7, 4, 8),
                new Triangle("Green", 10, 8, 11)};

        double sumArea = 0;
        double sumCircleArea = 0;
        double sumTriangleArea = 0;
        double sumRectArea = 0;

        for (Shape all : shapes) {
            double square = all.calcArea();
            System.out.println(Arrays.toString(new Shape[]{all}));
            System.out.println(all.calcArea());
            sumArea += square;
            if(all instanceof Circle) {
                sumCircleArea += square;
            }
            else if(all instanceof Triangle) {
                sumTriangleArea += square;
            }
            else if(all instanceof Rectangle) {
                sumRectArea += square;
            }
        }
        System.out.println("Total area is: " + sumArea);
        System.out.println("Total area Circle: " + sumCircleArea);
        System.out.println("Total area Triangle: " + sumTriangleArea);
        System.out.println("Total area Rectangle: " + sumRectArea);

        for(Shape shape : shapes) {
            shape.draw();
        }
        System.out.println("===========================");

        Rectangle[] arr2 = {new Rectangle("Red", 14, 17), new Rectangle("Red", 18, 19),
        new Rectangle("Red", 20, 22), new Rectangle("Red", 21, 19),
        new Rectangle("Red", 17, 17), new Rectangle("Red", 18, 15)};

        Arrays.sort(arr2);

        for(Rectangle rec : arr2) {
            rec.draw();
        }
    }

}
