package labWork2_8;

public class Triangle extends  Shape implements Comparable{
    private double a, b, c;

    public Triangle() {

    }

    public Triangle(String shapeColor, double a, double b, double c) {
        super(shapeColor);
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calcArea() {
        double s;
        s = (a + b + c) / 2;
        double area = Math.sqrt(s*(s - a)*(s - b)*(s - c));
        return area;
    }

    @Override
    public String toString() {
        return "This is Triangle, color: " + getShapeColor() + " a=" + a + " b=" + b + " c=" + c;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
