package labWork2_8;

public class Rectangle extends Shape implements Comparable {
    private double width;
    private double height;

    public Rectangle() {

    }

    public Rectangle(String shapeColor, double width, double height) {
        super(shapeColor);
        this.width = width;
        this.height = height;
    }

    @Override
    public double calcArea() {
        double area = width * height;
        return area;
    }

    @Override
    public String toString() {
        return "This is Rectangle, color: " + getShapeColor() + ", width=" + width + " height=" + height;
    }

    @Override
    public int compareTo(Object o) {
        Rectangle that = (Rectangle) o;
        String colorOfThat = that.getShapeColor();
        String colorOfThis = this.getShapeColor();
        return colorOfThis.compareTo(colorOfThat);
    }
}
