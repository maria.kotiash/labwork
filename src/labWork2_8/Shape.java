package labWork2_8;


public abstract class Shape implements Drawable {
    private String shapeColor;

    public Shape () {

    }

    public String getShapeColor() {
        return shapeColor;
    }

    public Shape(String shapeColor) {
        this.shapeColor = shapeColor;
    }

    public abstract double  calcArea();

    @Override
    public String toString() {
        return "Shape, color is: " + shapeColor;
    }

    @Override
    public void draw() {
        System.out.println(toString());
    }

}
