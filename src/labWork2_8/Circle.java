package labWork2_8;

public class Circle extends Shape implements Comparable {
    private double radius;

    public Circle(String shapeColor) {
        super(shapeColor);
    }

    @Override
    public double calcArea() {
        double area = Math.PI * radius * radius;
        return area;
    }

    public Circle(String shapeColor, double radius) {
        super(shapeColor);
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "This is Circle, color: " + getShapeColor() + ", radius = " + radius;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
