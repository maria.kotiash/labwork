package labWork1_6;

import java.util.Arrays;

public class BinSearch {
    public static void main(String[] args) {
        int[] numbers = new int[16];
        int index = 0;
        for(int i = 0; i <= 30; i += 2 ){
            numbers[index++] = i;
        }

        Arrays.sort(numbers);
        System.out.println("Sorted array - " + Arrays.toString(numbers));

        int value = 12;
        int binValue = Arrays.binarySearch(numbers, value);
        System.out.println("Your binary index is: " + binValue);
    }
}
