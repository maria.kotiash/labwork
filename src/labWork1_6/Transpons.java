package labWork1_6;

import java.util.Arrays;

public class Transpons {
    public static void main(String[] args) {
        int[][] matrix = {{1, 2, 3, 4},{5, 6, 7, 8}, {9, 10, 11, 12}, {13, 14, 15, 16}};
        for(int[] rov :matrix) {
            System.out.println(Arrays.toString(rov));
        }
        int[][] transmatrix = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0;  j< matrix[0].length; j++) {
                transmatrix[j][i] = matrix [i][j];
            }
        }
        for( int[] rov : transmatrix){
            System.out.println(Arrays.toString(rov));
        }
    }
}
