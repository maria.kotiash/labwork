package labWork1_6;

import java.util.Arrays;

public class MaxMinAverage {
    public static void main(String[] args) {
        int[] m = new int[] { 10, 21, 5, 22, 9, 29, 25, 22, 11, 14, 8, 14 };
Arrays.sort(m);
        System.out.println("Min value is: " + m[0]);
        System.out.println("Max value is: " + m[11]);

        int sum = 0;
        for (int value : m) {
            sum += value;
        }
        System.out.println("Average is: " + sum / m.length);
    }
}
