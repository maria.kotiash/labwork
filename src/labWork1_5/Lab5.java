package labWork1_5;

public class Lab5 {
    public static void main(String[] args) {
        int n = 10;
        int sum = 0;
        for(int i = 1; i <= n; i++){
            sum += i;
        }
        System.out.println("The sum is: " + sum);
        System.out.println("The Avg is: " + sum / n);
    }
}
