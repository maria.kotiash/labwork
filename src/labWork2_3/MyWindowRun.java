package labWork2_3;

public class MyWindowRun {
    public static void main(String[] args) {
        MyWindow window = new MyWindow(5.5, 7.8, 1, "Red", true);
        MyWindow window2 = new MyWindow(6.5, 8.5, 2, "Green", false);
        MyWindow window3 = new MyWindow(7.5, 6.8, 3, "Brown", false);
        MyWindow window4 = new MyWindow(4.5, 7, 4, "Black", true);
        MyWindow window5 = new MyWindow(6, 9.6,5, "White", true);
        MyWindow w = new MyWindow();

        MyWindow[] windows = {new MyWindow(7, 9), new MyWindow(6, 9, 4), new MyWindow(4, 7, 3, "Black"), new MyWindow(8, 10, 5, "White", true)};


        for(MyWindow all : windows ){
            all.printFields();
        }
    }
}
