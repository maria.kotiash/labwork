package labWork2_7;

import java.util.Objects;

public class Monitor extends Device {
    int resulutionX;
    int resulutionY;

    public Monitor() {

    }

    public Monitor(int resulutionX, int resulutionY) {
        this.resulutionX = resulutionX;
        this.resulutionY = resulutionY;
    }



    public int getResulutionX() {
        return resulutionX;
    }

    public void setResulutionX(int resulutionX) {
        this.resulutionX = resulutionX;
    }

    public int getResulutionY() {
        return resulutionY;
    }

    public void setResulutionY(int resulutionY) {
        this.resulutionY = resulutionY;
    }

    @Override
    public String toString() {
        return "Monitor{" +
                "resulutionX=" + resulutionX +
                ", resulutionY=" + resulutionY +
                ", manufacturer='" + manufacturer + '\'' +
                ", price=" + price +
                ", serialNumber='" + serialNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Monitor monitor = (Monitor) o;
        return resulutionX == monitor.resulutionX &&
                resulutionY == monitor.resulutionY;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), resulutionX, resulutionY);
    }

}
