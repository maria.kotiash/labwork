package labWork2_7;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Device[] device = new Device[] {new Device("HP", 500, "AR4674")};
        Device[] monitor = new Monitor[] {(new Monitor(1024, 700)), new Monitor(800, 600)};
        Device[] ethernet = new EthernetAdapter[] {new EthernetAdapter(3673,"Adapter")};

        System.out.println(Arrays.toString(device));
        System.out.println(Arrays.toString(monitor));
        System.out.println(Arrays.toString(ethernet));

    }

}
