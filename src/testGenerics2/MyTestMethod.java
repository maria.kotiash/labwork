package testGenerics2;

import java.math.BigDecimal;

public class MyTestMethod {
    static <T extends Number> long calcNum(T[] array, T maxElement){
        long count = 0;
        for(T t : array){
            if(t.doubleValue() > maxElement.doubleValue()){
                count++;
            }
        }
        return count;
    }

    static <T extends Number> T calcSum(T[] array, T maxValue){
        BigDecimal sum = new BigDecimal(0);
        for(T t : array){
            if(t.doubleValue() > maxValue.doubleValue()){
                sum = sum.add(new BigDecimal(t.doubleValue()));
            }
        }
        return (T) sum;
    }

}
