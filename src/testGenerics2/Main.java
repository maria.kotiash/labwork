package testGenerics2;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        Integer[] integers = new Integer[10];
        Double[] doubles = new Double[10];

        Random random = new Random();
        for (int j = 0; j < doubles.length; j++) {
           doubles[j] = random.nextDouble();
        }


        ThreadLocalRandom random1 = ThreadLocalRandom.current();
        for(int i = 0; i < integers.length; i++){
            integers[i] = random1.nextInt(20);
        }
for(Double all : doubles){
    System.out.println(all.toString());
}
        System.out.println(MyTestMethod.calcNum(integers, 5));
        System.out.println(MyTestMethod.calcNum(doubles, 5));
    }
}
